import express, { NextFunction, Request, Response } from 'express'
import doctorsController from './doctors/doctors.controller'
import { HttpError } from './http-error'

const app = express()

app.use('/doctors', doctorsController)

app.use(catchAll)
app.use(errorHandler)

export default app

function catchAll(_req: Request, _res: Response, next: NextFunction) {
  next(new HttpError(404, 'Endpoint not found'))
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function errorHandler(error: Error | HttpError, _req: Request, res: Response, _next: NextFunction) {
  const message = error.message
  if (error instanceof HttpError) {
    res.status(error.httpCode).json({ message })
  } else {
    res.status(500).json({ message })
  }
}
