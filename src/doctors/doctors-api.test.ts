import { expect } from 'chai'
import supertest from 'supertest'
import app from '../app'
import { clearAllDoctors, getDoctorById, insertNewDoctor } from './doctors.repository'
import { IDoctorDTO } from './types/IDoctorDTO'
import { INewDoctorDTO } from './types/INewDoctorDTO'

describe('Doctors API', async () => {
  beforeEach('clear doctors database', async () => {
    await clearAllDoctors()
  })

  describe('Get all doctors', () => {
    it('Should return an empty list of doctors', async () => {
      const response = await supertest(app).get('/doctors')
      expect(response.body).to.be.an('array').that.is.empty
    })

    it('Should return a list of 2 doctors', async () => {
      await insertNewDoctor({ name: 'Homer', licenseYear: 1984 })
      await insertNewDoctor({ name: 'Lisa', licenseYear: 2002 })

      const res = await supertest(app).get('/doctors').expect(200)
      expect(res.body).to.be.an('array').of.length(2)
    })
  })

  describe.skip('Get single doctor', () => {
    it('Should return the doctor', async () => {
      const doctor = await insertNewDoctor({ name: 'Bernie', licenseYear: 1977 })

      const response = await supertest(app)
        .get('/doctors/' + doctor.id)
        .expect(200)

      expect(response.body).to.be.an('object')
      expect(response.body).to.have.property('name', doctor.name)
    })

    it('Should throw error if doctor does not exist', async () => {
      const nonExistingId = '754vo754o7v46o'

      const response = await supertest(app)
        .get('/doctors/' + nonExistingId)
        .expect(404)

      expect(response.body).to.have.property('message', 'Could not find doctor with id: ' + nonExistingId)
    })
  })

  describe.skip('Insert new doctor', async () => {
    it('Should return the created doctor', async () => {
      const payload: INewDoctorDTO = { name: 'Bart', licenseYear: 2000 }

      const response = await supertest(app).post('/doctors').send(payload).expect(201)
      const createdDoctor: IDoctorDTO = response.body

      expect(createdDoctor).to.have.property('id').that.is.a('string')
      expect(createdDoctor).to.have.property('name', payload.name)
      expect(createdDoctor).to.have.property('licenseYear', payload.licenseYear)
      expect(createdDoctor).to.have.property('createdAt')
    })

    it('Should throw error if license year is in the future', async () => {
      const payload: INewDoctorDTO = { name: 'Maggie', licenseYear: 2030 }
      const response = await supertest(app).post('/doctors').send(payload).expect(400)
      expect(response.body).to.have.property('message', 'License year cannot be in the future')
    })
  })

  describe.skip('Delete a doctor', async () => {
    it('Should delete a doctor', async () => {
      const doctor = await insertNewDoctor({ name: 'Bernie', licenseYear: 1977 })

      await supertest(app)
        .delete('/doctors/' + doctor.id)
        .expect(204)

      const deletedDoctor = await getDoctorById(doctor.id)
      expect(deletedDoctor).to.be.undefined
    })
  })
})
