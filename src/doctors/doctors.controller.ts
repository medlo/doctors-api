import { Router } from 'express'
import { getAllDoctors } from './doctors.repository'
import { mapDoctorToDTO } from './doctors.utils'

const router = Router()

router.get('/', async (_req, res, next) => {
  try {
    const doctors = await getAllDoctors()
    const dtos = doctors.map(mapDoctorToDTO)
    res.json(dtos)
  } catch (err) {
    next(err)
  }
})

export default router
