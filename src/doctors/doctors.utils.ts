import { IDoctor } from './types/IDoctor'
import { IDoctorDTO } from './types/IDoctorDTO'

export const mapDoctorToDTO = (doctor: IDoctor): IDoctorDTO => ({
  id: doctor.id,
  name: doctor.name,
  licenseYear: doctor.licenseYear,
  createdAt: doctor.createdAt.toJSON()
})
