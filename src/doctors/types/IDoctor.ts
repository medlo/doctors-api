export interface IDoctor {
  id: string
  name: string
  licenseYear: number
  createdAt: Date
}
