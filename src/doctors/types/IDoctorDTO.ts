export interface IDoctorDTO {
  id: string
  name: string
  licenseYear: number
  createdAt: string
}
