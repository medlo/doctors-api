export interface INewDoctorDTO {
  name: string
  licenseYear: number
}
