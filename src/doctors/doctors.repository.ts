import { IDoctor } from './types/IDoctor'
import { INewDoctorDTO } from './types/INewDoctorDTO'
import crypto from 'node:crypto'

const doctors: IDoctor[] = []

export function getAllDoctors(): Promise<IDoctor[]> {
  return Promise.resolve(doctors)
}

export function getDoctorById(doctorId: string): Promise<IDoctor | undefined> {
  return Promise.resolve(doctors.find(d => d.id === doctorId))
}

export function insertNewDoctor(newDoctor: INewDoctorDTO): Promise<IDoctor> {
  const id = crypto.randomUUID()
  const createdAt = new Date()
  const doctor: IDoctor = { id, createdAt, ...newDoctor }
  doctors.push(doctor)
  return Promise.resolve(doctor)
}

export function deleteDoctorById(doctorId: string): Promise<number> {
  const index = doctors.findIndex(d => d.id === doctorId)
  if (index === -1) return Promise.resolve(0)

  doctors.splice(index, 1)
  return Promise.resolve(1)
}

export function clearAllDoctors(): Promise<void> {
  doctors.length = 0
  return Promise.resolve()
}
